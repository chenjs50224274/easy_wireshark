//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 Sniffer.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SNIFFER_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_DORPLIST1                   1000
#define IDC_DORPLIST2                   1001
#define IDC_BUTTON_STAR                 1002
#define IDC_BUTTON_END                  1003
#define IDC_TREE1                       1005
#define IDC_EDIT1                       1006
#define IDC_BUTTON_READ                 1007
#define IDC_BUTTON_SAVE                 1008
#define IDC_EDIT_IPV4                   1009
#define IDC_LIST2                       1010
#define IDC_EDIT_IPV6                   1032
#define IDC_EDIT_OTHER                  1033
#define IDC_EDIT_COUNT                  1034
#define IDC_EDIT_TCP                    1035
#define IDC_EDIT_UDP                    1036
#define IDC_EDIT_ICMP                   1037
#define IDC_EDIT_HTTP                   1038
#define IDC_EDIT_DNS                    1039
#define IDC_EDIT_FTP                    1040
#define IDC_EDIT_ICMPv6                 1041

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
